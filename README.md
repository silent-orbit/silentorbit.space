# Silent Orbit  

Silent Orbit provides the latest space related news, in a clear and concise manner. 

# Help

If you're looking for something to do, why not help us by submitting articles, and information to our open database.

We're looking to collate information about space organisations, as well as space craft. If you feel up to it why not volunteer to 'adopt' a craft. This would involve  writing reports on the current events regarding it, and maintaining the related pages on Silent Orbit. 

> Silent Orbit uses a tool called [Hugo](https://gohugo.io/) to create the website, you'll need to [install](https://gohugo.io/getting-started/installing/) this before you can help!

To create a new entry for a craft, or organisation:

	hugo new craft/iss/index.md
	hugo new org/esa/index.md

This will create an empty template, ready to be completed. Any images can be included within that folder, mandatory images are:
	
	logo[.png/jpg]

# Issues

Take a look at the [issue tracker](https://gitlab.com/PMaynard/silentorbit.space/issues) for anything you might be able to help with. This keeps track of milestones and tasks for the site. Currently we're aiming to have a first release on the **31st of March 2019**

# Draft Content

Content marked as draft means that there is still work left to be completed. This could be in the form of new content or proof reading. [Click here](https://gitlab.com/search?group_id=&project_id=7417615&repository_ref=&scope=blobs&search=draft%3A+true+) for a list of draft content, ignore "archetypes" as this is just used for templates.