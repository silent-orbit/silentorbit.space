---
title: Silent Orbit
---

This site is dedicated to providing clear and concise information about current space related activities. It intends to loosely follow these three objectives:

- Cover the most recent space related news and events.
- Create a corpus of information regarding current space activities, including organisations and operations.
- Provide free access to the corpus of data for future projects and activities.

This site is currently a work in progress, and development of the site is ongoing. The current plan is to be operational early 2019.